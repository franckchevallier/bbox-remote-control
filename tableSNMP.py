#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

table = {
		"1":50,
		"2":51,
		"3":52,
		"4":53,
		"5":54,
		"6":55,
		"7":56,
		"8":57,
		"9":58,
		"0":59,
		"Chaine+":25,
		"Chaine-":26,
		"VolumeUp":27,
		"VolumeDown":28,
		"Ok":7,
		"Haut":5,
		"Gauche":6,
		"Bas":9,
		"Droite":8,
		"Mute":32,
		"Retour":13,
		"Quitter":16,
		"VOD":30,
		"M@TV":46,
		"Infos":15,
		"Maison":12,
		"Liste":45,
		"Guide":31,
		"Rewind":18,
		"Play/Pause":23,
		"Stop":20,
		"Rec":24,
		"Forwind":19,
		"Marche/Arret":0
	}

def getCode(str):
	return table[str]
