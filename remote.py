#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import Tkinter
import socket

from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.proto import rfc1902

from tableSNMP import getCode

class remote_tk(Tkinter.Tk):

	#define the oid and the ip of the box
	oid = "1.3.6.1.4.1.8711.101.13.1.3.28.0"
	ip = "192.168.1.250"

	def __init__(self,parent):
		Tkinter.Tk.__init__(self,parent)
		self.parent = parent
		self.initialize()

	def initialize(self):
		#create the GUI
		self.grid()

		#first row
		line=0
		label = Tkinter.Label(self, anchor="w", fg="white", bg="blue", text="MY REMOTE")
		label.grid(column=0, row=line, columnspan=3, sticky='EW')

		#second row
		line=line+1
		self.createButton("Marche/Arret","Marche/Arret",0,line,3)

		#third row
		line=line+1
		j=1
		for i in range(1,10):
			button = Tkinter.Button(self, text=i, command=(lambda i: lambda: self.OnButtonClick(str(i)))(i))
			button.grid(column=(i-j),row=line)
			if (i%3)==0 or (i%6)==0 or (i%9)==0:
				line=line+1
				j=j+3

		#sixth row
		line=line+1
		self.createButton("V+", "VolumeUp", 1, line)
		self.createButton("0", "0", 2, line)

		#seventh row
		line=line+1
		self.createButton("P-", "Chaine-", 0, line)
		self.createButton("Mute", "Mute", 1, line)
		self.createButton("P+", "Chaine+", 2, line)

		#eighth row
		line=line+1
		self.createButton("V-", "VolumeDown", 1, line)

		self.resizable(False,False)

	def createButton(self, btnText, str, btnColumn, btnRow, btnColumnspan=1):
		button = Tkinter.Button(self, text=btnText, command=lambda:self.OnButtonClick(str))
		button.grid(column=btnColumn,row=btnRow,columnspan=btnColumnspan)
		return 

	#send message to the box
	def OnButtonClick(self, str):
		errorIndication, errorStatus, errorIndex, varBinding = cmdgen.CommandGenerator().setCmd(
			cmdgen.CommunityData("public", mpModel=0),
			cmdgen.UdpTransportTarget((self.ip, 161)),
			(self.oid, rfc1902.OctetString(getCode(str))),
		)

		print errorIndication
		print varBinding
		print "You clicked the button !"

if __name__ == "__main__":
	app = remote_tk(None)
	app.title('remote')
	app.mainloop()
