#REMOTE CONTROL FOR BBOX

##Simple remote control for your BBOX sensation

###To run
Assuming you have python and the pysnmp library installed on your computer:
`python remote.py`

###To configure
You will probably have to configure the program to fit your own box configuration. Normally you will not have to modify the oid, but certainly the IP address of the box. Edit the remote.py file:
`ip = your-own-ip-address`
